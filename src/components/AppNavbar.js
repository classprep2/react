import { Fragment, useContext } from 'react';

import {Nav,Navbar,Container} from 'react-bootstrap';

import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
export default function AppNavbar() {

    const { user } = useContext(UserContext);
    console.log(user)
    
    return (
        <Navbar bg="light" expand="lg">
            <Container fluid>                
                <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                        {(user.id !== null) ? 
                                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                            : 
                                <Fragment>
                                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                                </Fragment>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
