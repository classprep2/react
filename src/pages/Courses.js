import { useEffect, useState, useContext } from "react";

import { Navigate } from "react-router-dom";

import CourseCard from "../components/CourseCard";
import UserContext from "../UserContext";

export default function Courses() {

	const { user } = useContext(UserContext);

	
		// state    // setter function
	const [courses, setCourses] = useState([]);

	useEffect(() =>{
		// Will retrieve all the active courses
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course =>{
				return(
					<CourseCard key={course._id} courseProp={course}/>
				);
			}));
		})
	}, []);



	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:	
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}